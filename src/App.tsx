import React, {FC, useState} from 'react';
import Users from "./components/Users/Users";
import {useAppDispatch, useAppSelector} from "./store";
import {getUsers} from "./store/users";
import {useMount} from "./hooks";
import Header from "./components/Header/Header";
import styles from './App.module.scss';
import SearchUser from "./components/SearchUser/SearchUser";

const App: FC = () => {
  const dispatch = useAppDispatch();
  const {users, resultNumber, seed} = useAppSelector((state) => state.users);
  const [test, setTest] = useState(false);

  useMount(() => {
    dispatch(getUsers({resultNumber, page: 0, seed}))
  })

  return (
      <div className={styles.wrapper}>
          <Header />
          <div className={styles.search}>
              <SearchUser />
          </div>
          {users && <Users users={users} />}
      </div>
  )
}

export default App;
