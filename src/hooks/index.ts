import { useEffect, useRef} from "react";

export const useMount = (callback: () => void) => {
   const isMount = useRef(false);

   useEffect(() => {
       if (isMount.current) return;
       isMount.current = true;
       callback()
   },[])
}