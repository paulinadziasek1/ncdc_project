import React, {FC} from "react";
import SearchInput from "../../atoms/Form/SearchInput/SearchInput";
import styles from "./SearchUser.module.scss";

const SearchUser: FC = () => {
    return (
        <SearchInput searchIcon={true} placeholder="Search by seed" className={styles.searchInput} />
    )
}

export default SearchUser;