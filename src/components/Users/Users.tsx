import React, {FC} from "react";
import {UsersT} from "../../types";
import styles from './Users.module.scss';
import {useAppDispatch, useAppSelector} from "../../store";
import {addPage, getUsers} from "../../store/users";
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "../Loader/Loader";
import UserBox from "./UserBox/UserBox";

interface IUsers {
    users: UsersT;
}

const Users: FC<IUsers> = ({users}) => {
    const dispatch = useAppDispatch();
    const {page, resultNumber, seed} = useAppSelector(state => state.users);

    const fetchData = () => {
        dispatch(addPage())
        dispatch(getUsers({resultNumber, page, seed}))
    }

    return (
        <InfiniteScroll
            dataLength={users.results.length}
            next={fetchData}
            hasMore={true}
            loader={<Loader />}
        >
            <div className={styles.wrapper}>
                {users.results?.map((data, index) => (
                    <UserBox key={`${data.login.uuid}${index}`} data={data} index={index} />
                ))}
            </div>
        </InfiniteScroll>
    )
}

export default Users;