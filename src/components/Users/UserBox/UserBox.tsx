import React, {FC} from "react";
import styles from "./UserBox.module.scss";
import {ResultT} from "../../../types";

interface IUserBox {
    index: number,
    data: ResultT
}

const UserBox: FC<IUserBox> = ({data, index}) => {
    return (
        <div className={styles.box} key={`${data.login.uuid}${index}`}>
            <img className={styles.photo} alt="" src={data.picture.large} />
            <div className={styles.description}>
                <span>{data.name.title} {data.name.first} {data.name.last}</span>
            </div>
            <div className={styles.country}>
                <p>{data.location.country}</p>
            </div>
        </div>
    )
}

export default UserBox;