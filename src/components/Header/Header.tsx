import {FC} from "react";
import styles from './Header.module.scss';

const Header: FC = () => (
    <div className={styles.wrapper}>
        <h1>MY AWESOME APP</h1>
    </div>
)

export default Header;