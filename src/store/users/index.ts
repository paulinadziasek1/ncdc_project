import {createSlice, PayloadAction, createAsyncThunk} from '@reduxjs/toolkit';
import {UsersT} from "../../types";
import {api} from "../../api/Api";

interface IUsersGetUserReq {
    resultNumber: number;
    page: number;
    seed: string;
}

interface IInitialState extends IUsersGetUserReq {
    users: UsersT | null,
    pending: boolean,
    error: boolean
}

export const getUsers = createAsyncThunk(
    'users/getUsers',
    async ({resultNumber, page, seed} : IUsersGetUserReq) => {
    const response = await api.get(`?results=${resultNumber}&seed=${seed}&page=${page}`);
    return response.data;
})

const initialState : IInitialState = {
    users: null,
    pending: false,
    error: false,
    resultNumber: 25,
    page: 0,
    seed: ''
}

const users = createSlice({
    name: 'users',
    initialState,
    reducers: {
        addPage: (state) => {
            state.page += 1;
        },
        setSeed: (state, action: PayloadAction<string>) => {
            state.users = null;
            state.seed = action.payload;
        }
    },
    extraReducers: (build) => {
        build
            .addCase(getUsers.pending, (state) => {
                state.pending = true;
                state.error = false;
            })
            .addCase(getUsers.fulfilled, (state, action: PayloadAction<UsersT>) => {
                state.pending = false;
                state.error = false;
                if (!state.users) {
                    state.users = action.payload;
                } else {
                    state.users.results = [...state.users.results, ...action.payload.results]
                }
            })
            .addCase(getUsers.rejected, (state) => {
                state.pending = false;
                state.error = true;
            })
    }
})

export const {addPage, setSeed} = users.actions;
export default users.reducer;