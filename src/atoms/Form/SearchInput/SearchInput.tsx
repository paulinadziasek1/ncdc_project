import {FC, FormEvent, useCallback, useEffect, useState} from "react";
import styles from './SearchInput.module.scss';
import {ReactComponent as SearchIcon} from "../../../assets/icons/search-icon.svg";
import cn from "classnames";
import {setSeed, getUsers} from "../../../store/users";
import {useAppDispatch, useAppSelector} from "../../../store";
import debounce from 'lodash.debounce';

interface ISearchInput {
    className?: string;
    placeholder?: string;
    searchIcon?: boolean;
}

const SearchInput: FC<ISearchInput> = ({className, placeholder, searchIcon= false}) => {
    const dispatch = useAppDispatch();
    const {page, resultNumber, pending} = useAppSelector(state => state.users);
    const [searchValue, setSearchValue] = useState('');

    const searchSeed = useCallback((searchValue: string) => {
        dispatch(setSeed(searchValue))
        dispatch(getUsers({resultNumber, page, seed: searchValue}));
    },[dispatch, resultNumber, page])


    useEffect(() => {
        const debounce = setTimeout(() => {
            clearTimeout(debounce);
            searchSeed(searchValue);
        }, 300)
        return () => clearTimeout(debounce);
    },[searchValue])


    return (
        <div className={cn(styles.wrapper, className)}>
            {searchIcon && <SearchIcon />}
            <input
                value={searchValue}
                onChange={(e: FormEvent<HTMLInputElement>) => setSearchValue(e.currentTarget.value)}
                placeholder={placeholder}
            />
        </div>
    )
}

export default SearchInput;