export type ResultT = {
    picture: {
        large: string
    },
    name: {
        first: string,
        last: string,
        title: string
    },
    location: {
        country: string
    },
    login: {
        uuid: string
    }
}

export type UsersT = {
    info: {
        seed: string,
        page: number,
        result: number
    }
    results: ResultT[];
}